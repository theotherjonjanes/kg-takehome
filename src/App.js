import React, { Component } from 'react'
import axios from 'axios'
import './App.css'

class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [],
      day: 29,
      month: 3,
      year: 2014,
      games: [],
      singleGame: false,
      singleGameID: 0,
      stats: [],
      fetch: false,
      faveTeam: 'Blue Jays',
      homeStats: true,
      error: false
    }
    this.getData = this.getData.bind(this)
    this.gamesList = this.gamesList.bind(this)
    this.dayUp = this.dayUp.bind(this)
    this.dayDown = this.dayDown.bind(this)
    this.gameStats = this.gameStats.bind(this)
    this.dateSearch = this.dateSearch.bind(this)
  }

  render() {

    return (
      <div className="App">
        <p className="test-cases">
          <span onClick={() => { this.setState({ day: 29, month: 10, year: 2014, fetch: true }) }}>1</span>
          <span onClick={() => { this.setState({ day: 29, month: 3, year: 2014, fetch: true }) }}>2</span>
          <span onClick={() => { this.setState({ day: 15, month: 7, year: 2014, fetch: true }) }}>3</span>
          <span onClick={() => { this.setState({ day: 14, month: 7, year: 2014, fetch: true }) }}>4</span>
        </p>
        <DatePicker
          getData={this.getData}
          year={this.state.year}
          month={this.state.month}
          day={this.state.day}
          dayUp={this.dayUp}
          dayDown={this.dayDown}
          fetch={this.state.fetch}
          dSearch={this.dateSearch} />
        <div className="gamedata-container">
          <GamesList
            gameStats={this.gameStats}
            gamesList={this.gamesList}
            getData={this.getData} />
          <GameStats
            gameStats={this.gameStats} />
        </div>
      </div>
    )
  }

  // Lives in DatePicker
  getData() {
    // Get all the games for a single day
    let month = (this.state.month.toString().length === 1 ? `0${this.state.month}` : this.state.month)
    let day = (this.state.day.toString().length === 1 ? `0${this.state.day}` : this.state.day)
    axios.get(`http://gd2.mlb.com/components/game/mlb/year_${this.state.year}/month_${month}/day_${day}/master_scoreboard.json`)
      .then((result) => {
        console.log('Woot! Got Data! [getData()]')
        // console.log(result.data.data.games.game)
        this.setState({
          data: result.data.data.games,
          games: result.data.data.games.game,
          error: false,
          singleGame: false
        })
      })
      // Get the boxscore for each game that day
      .then((response) => {
        if (!this.state.games) {
          // console.log('No Games Today!')
        } else
          if (!Array.isArray(this.state.games)) {
            if (this.state.games.status.status === "Cancelled" || this.state.games.status.status === "Postponed") {
              // console.log('Cancelled/Postponed')
            } else {
              axios.get(`http://gd2.mlb.com${this.state.games.game_data_directory}/boxscore.json`)
                .then((result) => {
                  // console.log('Single Game')
                  // console.log(result.data.data.boxscore)
                  this.setState({
                    stats: result.data.data.boxscore
                  })
                })
            }
          } else {
            let promises = []
            for (let i = 0; i < this.state.games.length; ++i) {
              if (this.state.games[i].status.status === "Cancelled" || this.state.games[i].status.status === "Postponed") {
                // console.log('Cancelled/Postponed')
              } else {
                promises.push(axios.get(`http://gd2.mlb.com${this.state.games[i].game_data_directory}/boxscore.json`))
              }
            }
            axios.all(promises).then((results) => {
              console.log('Woot! Got Data! [getData()promises]')
              // console.log(results)
              this.setState({
                stats: results
              })
            })
              .catch((error) => {
                console.log('Boo! No Data! [getData()promises]')
                console.log(error)
              })
          }
      })
      // If date shows no data 
      .catch((error) => {
        // console.log('NO INFO')
        console.log('Boo! No Data! [getData()]')
        console.log(error)
        this.setState({
          data: [],
          games: [],
          error: true
        })
      })
    this.setState({
      fetch: false
    })
  }

  dayUp() {
    let d = this.state.day
    let m = this.state.month
    this.setState({
      day: this.state.day + 1,
      singleGame: false,
      fetch: true
    })
    if (d >= 31 && (m === 1 || m === 3 || m === 5 || m === 7 || m === 8 || m === 10)) {
      this.setState({
        day: 1,
        month: m + 1,
      })
    }
    if (d >= 30 && (m === 4 || m === 6 || m === 9 || m === 11)) {
      this.setState({
        day: 1,
        month: m + 1,
      })
    }
    if (d >= 28 && m === 2 && this.state.year % 4 !== 0) {
      this.setState({
        day: 1,
        month: m + 1,
      })
    }
    if (d >= 29 && m === 2) {
      this.setState({
        day: 1,
        month: m + 1,
      })
    }
    if (d >= 31 && m >= 12) {
      this.setState({
        day: 1,
        month: 1,
        year: this.state.year + 1
      })
    }
  }

  dayDown() {
    let d = this.state.day
    let m = this.state.month
    this.setState({
      day: d - 1,
      singleGame: false,
      fetch: true
    })
    if (d <= 1 && (m === 5 || m === 7 || m === 8 || m === 10 || m === 12)) {
      this.setState({
        day: 30,
        month: m - 1,
      })
    }
    if (d <= 1 && (m === 2 || m === 4 || m === 6 || m === 9 || m === 11)) {
      this.setState({
        day: 31,
        month: m - 1,
      })
    }
    if (d <= 1 && m === 3 && this.state.year % 4 === 0) {
      this.setState({
        day: 29,
        month: m - 1,
      })
    } else
      if (d <= 1 && m === 3) {
        this.setState({
          day: 28,
          month: m - 1,
        })
      }
    if (d <= 1 && m <= 1) {
      this.setState({
        day: 31,
        month: 12,
        year: this.state.year - 1
      })
    }
  }

  dateSearch(e) {
    e.preventDefault()
    let d = document.getElementById('dayDate').value
    let m = document.getElementById('monthDate').value
    let y = document.getElementById('yearDate').value
    this.setState({
      day: Number(d),
      month: Number(m),
      year: Number(y),
      fetch: true
    })
  }



  // Lives in GamesList
  gamesList() {
    let games = []
    let crown = <span>♕</span>
    let noScore = "-"
    // If no data is available from first API call
    if (this.state.error === true) {
      games =
        <div className="no-data-available" >
          <p>NO DATA AVAILABLE</p>
          <p id="no-data-available-sorry">Sorry!</p>
        </div>
      // If no games occur on selected day
    } else if (!this.state.games) {
      games =
        <div className="gameslist-no-games-today" >
          <p>NO GAMES TODAY</p>
          <p id="gameslist-no-games-today-sorry">Sorry!</p>
        </div>
      // If only a single game occurs on selected day
    } else if (!Array.isArray(this.state.games)) {
      games =
        <div className="gameslist-container" onMouseEnter={() => { this.setState({ singleGame: true }) }}>
          <div className="gameslist-teams-and-scores">
            <div className="gameslist-teams" >
              <p className="gameslist-teams-home">{this.state.games.home_team_name}{!this.state.games.linescore ? "" : this.state.games.linescore.r.home > this.state.games.linescore.r.away ? crown : ""}</p>
              <p className="gameslist-teams-away">{this.state.games.away_team_name}{!this.state.games.linescore ? "" : this.state.games.linescore.r.home < this.state.games.linescore.r.away ? crown : ""}</p>
            </div>
            <div className="gameslist-scores">
              <p className="gameslist-scores-home">{this.state.games.linescore ? this.state.games.linescore.r.home : noScore}</p>
              <p className="gameslist-scores-away">{this.state.games.linescore ? this.state.games.linescore.r.away : noScore}</p>
            </div>
          </div>
          <div className="gameslist-status">
            <p>{this.state.games.status.status}</p>
          </div>
          <div className="gameslist-venue">
            <p>{this.state.games.time_date_hm_lg}{this.state.games.home_time_zone} {this.state.games.ampm}</p>
            <p>at {this.state.games.venue}</p>
          </div>
        </div>
      // If multiple games occur on selected day
    } else {
      for (let i = 0; i < this.state.games.length; ++i) {
        // Go Jays!
        if ((this.state.games[i].home_team_name || this.state.games[i].away_team_name) === this.state.faveTeam) {
          games.unshift(
            <div className="gameslist-container" key={i} onMouseEnter={() => { this.setState({ singleGame: true, singleGameID: this.state.games[i].id }) }}>
              <div className="gameslist-teams-and-scores">
                <div className="gameslist-teams">
                  <p className="gameslist-teams-home" >{this.state.games[i].home_team_name}{!this.state.games[i].linescore ? "" : this.state.games[i].linescore.r.home > this.state.games[i].linescore.r.away ? crown : ""}</p>
                  <p className="gameslist-teams-away" >{this.state.games[i].away_team_name}{!this.state.games[i].linescore ? "" : this.state.games[i].linescore.r.home < this.state.games[i].linescore.r.away ? crown : ""}</p>
                </div>
                <div className="gameslist-scores">
                  <p className="gameslist-scores-home">{this.state.games[i].linescore ? this.state.games[i].linescore.r.home : noScore}</p>
                  <p className="gameslist-scores-away">{this.state.games[i].linescore ? this.state.games[i].linescore.r.away : noScore}</p>
                </div>
              </div>
              <div className="gameslist-status">
                <p>{this.state.games[i].status.status}</p>
              </div>
              <div className="gameslist-venue">
                <p>{this.state.games[i].time_date_hm_lg}{this.state.games[i].home_time_zone} {this.state.games[i].hm_lg_ampm}</p>
                <p>at {this.state.games[i].venue}</p>
              </div>
            </div>
          )
        } else {
          games.push(
            <div className="gameslist-container" key={i} onMouseEnter={() => { this.setState({ singleGame: true, singleGameID: this.state.games[i].id }) }}>
              <div className="gameslist-teams-and-scores">
                <div className="gameslist-teams">
                  <p className="gameslist-teams-home" >{this.state.games[i].home_team_name}{!this.state.games[i].linescore ? "" : this.state.games[i].linescore.r.home > this.state.games[i].linescore.r.away ? crown : ""}</p>
                  <p className="gameslist-teams-away" >{this.state.games[i].away_team_name}{!this.state.games[i].linescore ? "" : this.state.games[i].linescore.r.home < this.state.games[i].linescore.r.away ? crown : ""}</p>
                </div>
                <div className="gameslist-scores">
                  <p className="gameslist-scores-home">{this.state.games[i].linescore ? this.state.games[i].linescore.r.home : noScore}</p>
                  <p className="gameslist-scores-away">{this.state.games[i].linescore ? this.state.games[i].linescore.r.away : noScore}</p>
                </div>
              </div>
              <div className="gameslist-status">
                <p>{this.state.games[i].status.status}</p>
              </div>
              <div className="gameslist-venue">
                <p>{this.state.games[i].time_date_hm_lg}{this.state.games[i].home_time_zone} {this.state.games[i].hm_lg_ampm}</p>
                <p>at {this.state.games[i].venue}</p>
              </div>
            </div>
          )
        }
      }
    }
    return games
  }



  // Lives in GameStats
  gameStats() {
    let stats = []
    // If no stats exist
    if (this.state.singleGame === false) {
      return
      // If no games occur
    } else if (!this.state.games) {
      return
      // If single game occurs
    } else if (!Array.isArray(this.state.games)) {
      if (this.state.games.status.status === "Cancelled" || this.state.games.status.status === "Postponed") {
        // console.log('Cancelled/Postponed')
      } else {
        let innings = []
        let homeScore = []
        let awayScore = []
        let homeBatters = []
        let awayBatters = []
        for (let i = 0; i < this.state.stats.linescore.inning_line_score.length; ++i) {
          innings.push(<th key={i}>{this.state.stats.linescore.inning_line_score[i].inning}</th>)
        }
        for (let i = 0; i < this.state.stats.linescore.inning_line_score.length; ++i) {
          homeScore.push(<td key={i}>{this.state.stats.linescore.inning_line_score[i].home}</td>)
        }
        for (let i = 0; i < this.state.stats.linescore.inning_line_score.length; ++i) {
          awayScore.push(<td key={i}>{this.state.stats.linescore.inning_line_score[i].away}</td>)
        }
        for (let i = 0; i < this.state.stats.batting[0].batter.length; ++i) {
          homeBatters.push(<tr key={i}><td>{this.state.stats.batting[0].batter[i].name}</td>
            <td>{this.state.stats.batting[0].batter[i].ab}</td>
            <td>{this.state.stats.batting[0].batter[i].r}</td>
            <td>{this.state.stats.batting[0].batter[i].h}</td>
            <td>{this.state.stats.batting[0].batter[i].rbi}</td>
            <td>{this.state.stats.batting[0].batter[i].bb}</td>
            <td>{this.state.stats.batting[0].batter[i].so}</td>
            <td>{this.state.stats.batting[0].batter[i].avg}</td></tr>
          )
        }
        for (let i = 0; i < this.state.stats.batting[1].batter.length; ++i) {
          awayBatters.push(<tr key={i}><td>{this.state.stats.batting[1].batter[i].name}</td>
            <td>{this.state.stats.batting[1].batter[i].ab}</td>
            <td>{this.state.stats.batting[1].batter[i].r}</td>
            <td>{this.state.stats.batting[1].batter[i].h}</td>
            <td>{this.state.stats.batting[1].batter[i].rbi}</td>
            <td>{this.state.stats.batting[1].batter[i].bb}</td>
            <td>{this.state.stats.batting[1].batter[i].so}</td>
            <td>{this.state.stats.batting[1].batter[i].avg}</td></tr>
          )
        }
        stats =
          <div className="gamestats-container">
            <div className="gamestats-scoretable">
              <table>
                <tbody>
                  <tr>
                    <th></th>
                    {innings}
                    <th>R</th>
                    <th>H</th>
                    <th>E</th>
                  </tr>
                  <tr>
                    <th>{this.state.stats.home_team_code.toUpperCase()}</th>
                    {homeScore}
                    <td>{this.state.stats.linescore.home_team_runs}</td>
                    <td>{this.state.stats.linescore.home_team_hits}</td>
                    <td>{this.state.stats.linescore.home_team_errors}</td>
                  </tr>
                  <tr>
                    <th>{this.state.stats.away_team_code.toUpperCase()}</th>
                    {awayScore}
                    <td>{this.state.stats.linescore.away_team_runs}</td>
                    <td>{this.state.stats.linescore.away_team_hits}</td>
                    <td>{this.state.stats.linescore.away_team_errors}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="gamestats-teams">
              <p onMouseEnter={() => { this.setState({ homeStats: true }) }} className={this.state.homeStats ? "gamestats-teams-bold" : ""}>{this.state.stats.home_fname}</p>
              <hr />
              <p onMouseEnter={() => { this.setState({ homeStats: false }) }} className={!this.state.homeStats ? "gamestats-teams-bold" : ""}>{this.state.stats.away_fname}</p>
            </div>
            <div className="gamestats-batterstats">
              <table>
                <tbody>
                  <tr>
                    <th>NAME</th>
                    <th>AB</th>
                    <th>R</th>
                    <th>H</th>
                    <th>RBI</th>
                    <th>BB</th>
                    <th>SO</th>
                    <th>AVG</th>
                  </tr>
                  {this.state.homeStats ? homeBatters : awayBatters}
                </tbody>
              </table>
            </div>
          </div>
      }
      // If multiple games occur
    } else {
      for (let j = 0; j < this.state.stats.length; ++j) {
        if (this.state.stats[j].data.data.boxscore.game_id === this.state.singleGameID) {
          let innings = []
          let homeScore = []
          let awayScore = []
          let homeBatters = []
          let awayBatters = []
          for (let i = 0; i < this.state.stats[j].data.data.boxscore.linescore.inning_line_score.length; ++i) {
            innings.push(<th key={i}>{this.state.stats[j].data.data.boxscore.linescore.inning_line_score[i].inning}</th>)
          }
          for (let i = 0; i < this.state.stats[j].data.data.boxscore.linescore.inning_line_score.length; ++i) {
            homeScore.push(<td key={i}>{this.state.stats[j].data.data.boxscore.linescore.inning_line_score[i].home}</td>)
          }
          for (let i = 0; i < this.state.stats[j].data.data.boxscore.linescore.inning_line_score.length; ++i) {
            awayScore.push(<td key={i}>{this.state.stats[j].data.data.boxscore.linescore.inning_line_score[i].away}</td>)
          }
          for (let i = 0; i < this.state.stats[j].data.data.boxscore.batting[0].batter.length; ++i) {
            homeBatters.push(<tr key={i}><td>{this.state.stats[j].data.data.boxscore.batting[0].batter[i].name}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[0].batter[i].ab}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[0].batter[i].r}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[0].batter[i].h}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[0].batter[i].rbi}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[0].batter[i].bb}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[0].batter[i].so}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[0].batter[i].avg}</td></tr>
            )
          }
          for (let i = 0; i < this.state.stats[j].data.data.boxscore.batting[1].batter.length; ++i) {
            awayBatters.push(<tr key={i}><td>{this.state.stats[j].data.data.boxscore.batting[1].batter[i].name}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[1].batter[i].ab}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[1].batter[i].r}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[1].batter[i].h}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[1].batter[i].rbi}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[1].batter[i].bb}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[1].batter[i].so}</td>
              <td>{this.state.stats[j].data.data.boxscore.batting[1].batter[i].avg}</td></tr>
            )
          }
          stats =
            <div className="gamestats-container">
              <div className="gamestats-scoretable">
                <table>
                  <tbody>
                    <tr>
                      <th></th>
                      {innings}
                      <th>R</th>
                      <th>H</th>
                      <th>E</th>
                    </tr>
                    <tr>
                      <th>{this.state.stats[j].data.data.boxscore.home_team_code.toUpperCase()}</th>
                      {homeScore}
                      <td>{this.state.stats[j].data.data.boxscore.linescore.home_team_runs}</td>
                      <td>{this.state.stats[j].data.data.boxscore.linescore.home_team_hits}</td>
                      <td>{this.state.stats[j].data.data.boxscore.linescore.home_team_errors}</td>
                    </tr>
                    <tr>
                      <th>{this.state.stats[j].data.data.boxscore.away_team_code.toUpperCase()}</th>
                      {awayScore}
                      <td>{this.state.stats[j].data.data.boxscore.linescore.away_team_runs}</td>
                      <td>{this.state.stats[j].data.data.boxscore.linescore.away_team_hits}</td>
                      <td>{this.state.stats[j].data.data.boxscore.linescore.away_team_errors}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="gamestats-teams">
                <p><span onMouseEnter={() => { this.setState({ homeStats: true }) }} className={this.state.homeStats ? "gamestats-teams-bold" : ""}>{this.state.stats[j].data.data.boxscore.home_fname}</span></p>
                <hr />
                <p><span onMouseEnter={() => { this.setState({ homeStats: false }) }} className={!this.state.homeStats ? "gamestats-teams-bold" : ""}>{this.state.stats[j].data.data.boxscore.away_fname}</span></p>
              </div>
              <div className="gamestats-batterstats">
                <table>
                  <tbody>
                    <tr>
                      <th>NAME</th>
                      <th>AB</th>
                      <th>R</th>
                      <th>H</th>
                      <th>RBI</th>
                      <th>BB</th>
                      <th>SO</th>
                      <th>AVG</th>
                    </tr>
                    {this.state.homeStats ? homeBatters : awayBatters}
                  </tbody>
                </table>
              </div>
            </div>
        }
      }
    }
    return stats
  }

}



class DatePicker extends Component {
  render() {

    let month = (this.props.month.toString().length === 1 ? `0${this.props.month}` : this.props.month)
    let day = (this.props.day.toString().length === 1 ? `0${this.props.day}` : this.props.day)

    return (
      <div className="DatePicker" >
        <p><span onClick={this.props.dayDown}>← PREV DAY</span> {day}/{month}/{this.props.year} <span onClick={this.props.dayUp}>NEXT DAY →</span></p>
        <div className="datepicker-date">
          <form onSubmit={(e) => { this.props.dSearch(e) }}>
            <p><input type="number" id="dayDate" maxLength="2" minLength="2" required min="1" max="31" /> /
          <input type="number" id="monthDate" maxLength="2" minLength="2" required min="1" max="12" /> /
          <input type="number" id="yearDate" maxLength="4" minLength="4" required min="1839" max="2020" /></p>
            <p>dd / mm / yyyy</p>
            <p><button id="submit" type="submit">SUBMIT</button></p>
          </form>
        </div>
      </div>
    )
  }

  componentDidMount() {
    this.props.getData()
  }

  // Get list of games when date changes
  componentDidUpdate() {
    if (this.props.fetch) {
      this.props.getData()
    }
  }
}

class GamesList extends Component {
  render() {

    return (
      <div className="GamesList">
        <p className="gameslist-guide">HOVER FOR DETAILED STATS</p>
        {this.props.gamesList()}
      </div>
    )
  }

}

class GameStats extends Component {
  render() {

    return (
      <div className="GameStats">
        {this.props.gameStats()}
      </div>
    )
  }

}

export default App;
